package com.ghintech.prim.agrosilos.weightcontrol.callout;

import org.adempiere.base.IColumnCallout;
import org.adempiere.base.IColumnCalloutFactory;
import org.compiere.model.CalloutEngine;

public class CallOutFactory  extends CalloutEngine implements IColumnCalloutFactory{	
	@Override
	public IColumnCallout[] getColumnCallouts(String tableName, String columnName) {
		if(tableName.equals("PP_Order")){
			if (columnName.equalsIgnoreCase("XX_Ticket_Vigilancia_ID"))
				return new IColumnCallout[] {new Ticket_Vigilancia()};
			if (columnName.equalsIgnoreCase("DocumentQuality_ID"))
				return new IColumnCallout[] {new select_XX_QualityOrder()};
			if (columnName.equalsIgnoreCase("Weight"))
				return new IColumnCallout[] {new CalloutOrder()};
			if (columnName.equalsIgnoreCase("Weight2"))
				return new IColumnCallout[] {new CalloutOrder()};
		}
		
		return new IColumnCallout[0];
	}
}