package com.ghintech.prim.agrosilos.weightcontrol.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

public class CalloutOrder implements IColumnCallout{
		@Override
		public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,
				Object oldValue) {			
			
			
			
			BigDecimal pesoTara  = (BigDecimal)mTab.getValue("Weight");
			BigDecimal pesoBruto = (BigDecimal)mTab.getValue("Weight2");
			BigDecimal pesoNeto=BigDecimal.ZERO;
			if (pesoTara!=null && pesoBruto!=null)
				 pesoNeto = pesoBruto.subtract(pesoTara);
			mTab.setValue("Weight3",pesoNeto);
			return null;
		}
	}