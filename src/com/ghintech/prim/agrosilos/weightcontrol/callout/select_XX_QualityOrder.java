package com.ghintech.prim.agrosilos.weightcontrol.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.eevolution.model.X_PP_Order;

public class select_XX_QualityOrder implements IColumnCallout{

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,
			Object oldValue) {
		//	@script:groovy:Call_Pes_QO
		Integer m_XX_QualityOrder_ID = (Integer)value;
		if (m_XX_QualityOrder_ID == null || m_XX_QualityOrder_ID.intValue() == 0)
			return "";
		
		//	code

		X_PP_Order order = new X_PP_Order(ctx, m_XX_QualityOrder_ID.intValue(), null);
	    mTab.setValue("M_AttributeSetInstance_ID", order.getM_AttributeSetInstance_ID());
	    mTab.setValue("M_Product_ID", order.getM_Product_ID());
	    mTab.setValue("M_Shipper_ID", order.get_ValueAsInt("M_Shipper_ID"));
	    mTab.setValue("XX_Conductor_ID", order.get_ValueAsInt("XX_Conductor_ID"));
	    mTab.setValue("XX_Vehiculo_ID", order.get_ValueAsInt("XX_Vehiculo_ID"));
	    mTab.setValue("M_Warehouse_ID", order.getM_Warehouse_ID());
	    mTab.setValue("AD_Workflow_ID", order.getAD_Workflow_ID());
	    mTab.setValue("XX_Ticket_Vigilancia_ID", order.get_ValueAsInt("XX_Ticket_Vigilancia_ID"));
	    mTab.setValue("C_BPartner_ID", order.get_ValueAsInt("C_BPartner_ID"));
	    mTab.setValue("IsInclude", order.get_Value("IsInclude"));
	    mTab.setValue("C_UOM_ID", order.get_ValueAsInt("C_UOM_ID"));
		return null;
	}
	}