/**
 * 
 */
package com.ghintech.prim.agrosilos.weightcontrol.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Yamel Senih 27/06/2011, 02:57
 *
 */
public class MXXLoadOrder extends X_XX_LoadOrder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8637350886012568428L;
	
	public MXXLoadOrder(Properties ctx, int XX_LoadOrder_ID, String trxName) {
		super(ctx, XX_LoadOrder_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MXXLoadOrder(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

}
