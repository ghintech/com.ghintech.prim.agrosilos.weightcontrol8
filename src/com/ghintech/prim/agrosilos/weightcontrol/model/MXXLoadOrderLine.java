/**
 * 
 */
package com.ghintech.prim.agrosilos.weightcontrol.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Yamel Senih 27/06/2011, 02:57
 *
 */
public class MXXLoadOrderLine extends X_XX_LoadOrderLine {
	/**
	 * 
	 */
	private static final long serialVersionUID = -949207107600644343L;
	
	public MXXLoadOrderLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MXXLoadOrderLine(Properties ctx, int XX_LoadOrderLine_ID,
			String trxName) {
		super(ctx, XX_LoadOrderLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}
}
