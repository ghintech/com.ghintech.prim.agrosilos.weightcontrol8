package com.ghintech.prim.agrosilos.weightcontrol.model;

import java.sql.ResultSet;

import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

public class ModelFactory implements IModelFactory {

	@Override
	public Class<?> getClass(String tableName) {
        if ("XX_Ticket_Vigilancia".equals(tableName)) {
            return MXXTicketVigilancia.class;
        }
        return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
        if ("XX_Ticket_Vigilancia".equals(tableName)) {
            return new MXXTicketVigilancia(Env.getCtx(), Record_ID, trxName);
        }
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
        if ("XX_Ticket_Vigilancia".equals(tableName)) {
            return new MXXTicketVigilancia(Env.getCtx(), rs, trxName);
        }
		return null;
	}

}
