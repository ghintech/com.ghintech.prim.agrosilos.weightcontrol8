/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.ghintech.prim.agrosilos.weightcontrol.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_LoadOrder
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_LoadOrder extends PO implements I_XX_LoadOrder, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120403L;

    /** Standard Constructor */
    public X_XX_LoadOrder (Properties ctx, int XX_LoadOrder_ID, String trxName)
    {
      super (ctx, XX_LoadOrder_ID, trxName);
      /** if (XX_LoadOrder_ID == 0)
        {
			setDocumentNo (null);
			setM_Shipper_ID (0);
			setXX_Conductor_ID (0);
			setXX_LoadOrder_ID (0);
			setXX_Vehiculo_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_LoadOrder (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_LoadOrder[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_DocType getC_DocTypeOrder() throws RuntimeException
    {
		return (I_C_DocType)MTable.get(getCtx(), I_C_DocType.Table_Name)
			.getPO(getC_DocTypeOrder_ID(), get_TrxName());	}

	/** Set Tipo de Orden.
		@param C_DocTypeOrder_ID 
		Tipo de Orden de Venta
	  */
	public void setC_DocTypeOrder_ID (int C_DocTypeOrder_ID)
	{
		if (C_DocTypeOrder_ID < 1) 
			set_Value (COLUMNNAME_C_DocTypeOrder_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocTypeOrder_ID, Integer.valueOf(C_DocTypeOrder_ID));
	}

	/** Get Tipo de Orden.
		@return Tipo de Orden de Venta
	  */
	public int getC_DocTypeOrder_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocTypeOrder_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Capacity.
		@param Capacity Capacity	  */
	public void setCapacity (BigDecimal Capacity)
	{
		set_Value (COLUMNNAME_Capacity, Capacity);
	}

	/** Get Capacity.
		@return Capacity	  */
	public BigDecimal getCapacity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Capacity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Finish Date.
		@param DateFinish 
		Finish or (planned) completion date
	  */
	public void setDateFinish (Timestamp DateFinish)
	{
		set_Value (COLUMNNAME_DateFinish, DateFinish);
	}

	/** Get Finish Date.
		@return Finish or (planned) completion date
	  */
	public Timestamp getDateFinish () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateFinish);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_Value (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Driver Description.
		@param DriverDescription 
		Optional short description of the record
	  */
	public void setDriverDescription (String DriverDescription)
	{
		set_Value (COLUMNNAME_DriverDescription, DriverDescription);
	}

	/** Get Driver Description.
		@return Optional short description of the record
	  */
	public String getDriverDescription () 
	{
		return (String)get_Value(COLUMNNAME_DriverDescription);
	}

	/** Set Delivered.
		@param IsDelivered Delivered	  */
	public void setIsDelivered (boolean IsDelivered)
	{
		set_Value (COLUMNNAME_IsDelivered, Boolean.valueOf(IsDelivered));
	}

	/** Get Delivered.
		@return Delivered	  */
	public boolean isDelivered () 
	{
		Object oo = get_Value(COLUMNNAME_IsDelivered);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Invoiced.
		@param IsInvoiced 
		Is this invoiced?
	  */
	public void setIsInvoiced (boolean IsInvoiced)
	{
		set_Value (COLUMNNAME_IsInvoiced, Boolean.valueOf(IsInvoiced));
	}

	/** Get Invoiced.
		@return Is this invoiced?
	  */
	public boolean isInvoiced () 
	{
		Object oo = get_Value(COLUMNNAME_IsInvoiced);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public I_M_Locator getM_Locator() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_Value (COLUMNNAME_M_Locator_ID, null);
		else 
			set_Value (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Locator getM_LocatorTo() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_LocatorTo_ID(), get_TrxName());	}

	/** Set Locator To.
		@param M_LocatorTo_ID 
		Location inventory is moved to
	  */
	public void setM_LocatorTo_ID (int M_LocatorTo_ID)
	{
		if (M_LocatorTo_ID < 1) 
			set_Value (COLUMNNAME_M_LocatorTo_ID, null);
		else 
			set_Value (COLUMNNAME_M_LocatorTo_ID, Integer.valueOf(M_LocatorTo_ID));
	}

	/** Get Locator To.
		@return Location inventory is moved to
	  */
	public int getM_LocatorTo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_LocatorTo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Shipper getM_Shipper() throws RuntimeException
    {
		return (I_M_Shipper)MTable.get(getCtx(), I_M_Shipper.Table_Name)
			.getPO(getM_Shipper_ID(), get_TrxName());	}

	/** Set Shipper.
		@param M_Shipper_ID 
		Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID)
	{
		if (M_Shipper_ID < 1) 
			set_Value (COLUMNNAME_M_Shipper_ID, null);
		else 
			set_Value (COLUMNNAME_M_Shipper_ID, Integer.valueOf(M_Shipper_ID));
	}

	/** Get Shipper.
		@return Method or manner of product delivery
	  */
	public int getM_Shipper_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Shipper_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (I_M_Warehouse)MTable.get(getCtx(), I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_Value (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_Value (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Ship Date.
		@param ShipDate 
		Shipment Date/Time
	  */
	public void setShipDate (Timestamp ShipDate)
	{
		set_Value (COLUMNNAME_ShipDate, ShipDate);
	}

	/** Get Ship Date.
		@return Shipment Date/Time
	  */
	public Timestamp getShipDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ShipDate);
	}

	/** Set TotalWeight.
		@param TotalWeight TotalWeight	  */
	public void setTotalWeight (BigDecimal TotalWeight)
	{
		set_Value (COLUMNNAME_TotalWeight, TotalWeight);
	}

	/** Get TotalWeight.
		@return TotalWeight	  */
	public BigDecimal getTotalWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalWeight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getTotalWeight()));
    }

	/** Set Vehicle Description.
		@param VehicleDescription 
		Optional short description of the record
	  */
	public void setVehicleDescription (String VehicleDescription)
	{
		set_Value (COLUMNNAME_VehicleDescription, VehicleDescription);
	}

	/** Get Vehicle Description.
		@return Optional short description of the record
	  */
	public String getVehicleDescription () 
	{
		return (String)get_Value(COLUMNNAME_VehicleDescription);
	}

	/** XX_Annulled AD_Reference_ID=1000040 */
	public static final int XX_ANNULLED_AD_Reference_ID=1000040;
	/** Annulled Document = Y */
	public static final String XX_ANNULLED_AnnulledDocument = "Y";
	/** Annul Document = N */
	public static final String XX_ANNULLED_AnnulDocument = "N";
	/** Set Annulled.
		@param XX_Annulled Annulled	  */
	public void setXX_Annulled (String XX_Annulled)
	{

		set_Value (COLUMNNAME_XX_Annulled, XX_Annulled);
	}

	/** Get Annulled.
		@return Annulled	  */
	public String getXX_Annulled () 
	{
		return (String)get_Value(COLUMNNAME_XX_Annulled);
	}

	/** Set Conductor.
		@param XX_Conductor_ID Conductor	  */
	public void setXX_Conductor_ID (int XX_Conductor_ID)
	{
		if (XX_Conductor_ID < 1) 
			set_Value (COLUMNNAME_XX_Conductor_ID, null);
		else 
			set_Value (COLUMNNAME_XX_Conductor_ID, Integer.valueOf(XX_Conductor_ID));
	}

	/** Get Conductor.
		@return Conductor	  */
	public int getXX_Conductor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Conductor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Load Order.
		@param XX_LoadOrder_ID Load Order	  */
	public void setXX_LoadOrder_ID (int XX_LoadOrder_ID)
	{
		if (XX_LoadOrder_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_LoadOrder_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_LoadOrder_ID, Integer.valueOf(XX_LoadOrder_ID));
	}

	/** Get Load Order.
		@return Load Order	  */
	public int getXX_LoadOrder_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_LoadOrder_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Net Weight.
		@param XX_NetWeight Net Weight	  */
	public void setXX_NetWeight (BigDecimal XX_NetWeight)
	{
		set_Value (COLUMNNAME_XX_NetWeight, XX_NetWeight);
	}

	/** Get Net Weight.
		@return Net Weight	  */
	public BigDecimal getXX_NetWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_XX_NetWeight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_UOM getXX_Vehicle_UOM() throws RuntimeException
    {
		return (I_C_UOM)MTable.get(getCtx(), I_C_UOM.Table_Name)
			.getPO(getXX_Vehicle_UOM_ID(), get_TrxName());	}

	/** Set Vehicle Unit Measure.
		@param XX_Vehicle_UOM_ID Vehicle Unit Measure	  */
	public void setXX_Vehicle_UOM_ID (int XX_Vehicle_UOM_ID)
	{
		if (XX_Vehicle_UOM_ID < 1) 
			set_Value (COLUMNNAME_XX_Vehicle_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_XX_Vehicle_UOM_ID, Integer.valueOf(XX_Vehicle_UOM_ID));
	}

	/** Get Vehicle Unit Measure.
		@return Vehicle Unit Measure	  */
	public int getXX_Vehicle_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Vehicle_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Car.
		@param XX_Vehiculo_ID Car	  */
	public void setXX_Vehiculo_ID (int XX_Vehiculo_ID)
	{
		if (XX_Vehiculo_ID < 1) 
			set_Value (COLUMNNAME_XX_Vehiculo_ID, null);
		else 
			set_Value (COLUMNNAME_XX_Vehiculo_ID, Integer.valueOf(XX_Vehiculo_ID));
	}

	/** Get Car.
		@return Car	  */
	public int getXX_Vehiculo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Vehiculo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_UOM getXX_Work_UOM() throws RuntimeException
    {
		return (I_C_UOM)MTable.get(getCtx(), I_C_UOM.Table_Name)
			.getPO(getXX_Work_UOM_ID(), get_TrxName());	}

	/** Set Unit of Work.
		@param XX_Work_UOM_ID Unit of Work	  */
	public void setXX_Work_UOM_ID (int XX_Work_UOM_ID)
	{
		if (XX_Work_UOM_ID < 1) 
			set_Value (COLUMNNAME_XX_Work_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_XX_Work_UOM_ID, Integer.valueOf(XX_Work_UOM_ID));
	}

	/** Get Unit of Work.
		@return Unit of Work	  */
	public int getXX_Work_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Work_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Is Bulk.
		@param XXIsBulk Is Bulk	  */
	public void setXXIsBulk (boolean XXIsBulk)
	{
		set_Value (COLUMNNAME_XXIsBulk, Boolean.valueOf(XXIsBulk));
	}

	/** Get Is Bulk.
		@return Is Bulk	  */
	public boolean isXXIsBulk () 
	{
		Object oo = get_Value(COLUMNNAME_XXIsBulk);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Driver Released.
		@param XXIsDriverReleased Driver Released	  */
	public void setXXIsDriverReleased (boolean XXIsDriverReleased)
	{
		set_Value (COLUMNNAME_XXIsDriverReleased, Boolean.valueOf(XXIsDriverReleased));
	}

	/** Get Driver Released.
		@return Driver Released	  */
	public boolean isXXIsDriverReleased () 
	{
		Object oo = get_Value(COLUMNNAME_XXIsDriverReleased);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Internal Load.
		@param XXIsInternalLoad Is Internal Load	  */
	public void setXXIsInternalLoad (boolean XXIsInternalLoad)
	{
		set_Value (COLUMNNAME_XXIsInternalLoad, Boolean.valueOf(XXIsInternalLoad));
	}

	/** Get Is Internal Load.
		@return Is Internal Load	  */
	public boolean isXXIsInternalLoad () 
	{
		Object oo = get_Value(COLUMNNAME_XXIsInternalLoad);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Moved.
		@param XXIsMoved Is Moved	  */
	public void setXXIsMoved (boolean XXIsMoved)
	{
		set_Value (COLUMNNAME_XXIsMoved, Boolean.valueOf(XXIsMoved));
	}

	/** Get Is Moved.
		@return Is Moved	  */
	public boolean isXXIsMoved () 
	{
		Object oo = get_Value(COLUMNNAME_XXIsMoved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Vehicle Released.
		@param XXIsVehicleReleased Vehicle Released	  */
	public void setXXIsVehicleReleased (boolean XXIsVehicleReleased)
	{
		set_Value (COLUMNNAME_XXIsVehicleReleased, Boolean.valueOf(XXIsVehicleReleased));
	}

	/** Get Vehicle Released.
		@return Vehicle Released	  */
	public boolean isXXIsVehicleReleased () 
	{
		Object oo = get_Value(COLUMNNAME_XXIsVehicleReleased);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Weight Register.
		@param XXIsWeightRegister Is Weight Register	  */
	public void setXXIsWeightRegister (boolean XXIsWeightRegister)
	{
		set_Value (COLUMNNAME_XXIsWeightRegister, Boolean.valueOf(XXIsWeightRegister));
	}

	/** Get Is Weight Register.
		@return Is Weight Register	  */
	public boolean isXXIsWeightRegister () 
	{
		Object oo = get_Value(COLUMNNAME_XXIsWeightRegister);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}
}