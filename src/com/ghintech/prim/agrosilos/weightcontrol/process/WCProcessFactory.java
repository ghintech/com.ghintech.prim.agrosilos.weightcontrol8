package com.ghintech.prim.agrosilos.weightcontrol.process;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

public class WCProcessFactory implements IProcessFactory{

	@Override
	public ProcessCall newProcessInstance(String className) {
		// TODO Auto-generated method stub
		if(className.equals("com.ghintech.prim.agrosilos.weightcontrol.process.GetWeight"))
			return new GetWeight();
		return null;
	}

}
