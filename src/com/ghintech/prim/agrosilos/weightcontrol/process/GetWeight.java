package com.ghintech.prim.agrosilos.weightcontrol.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
//mport com.ghintech.agrosilos.pporder.model.MPPOrder;
import org.eevolution.model.X_PP_Order;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.math.BigDecimal;


public class GetWeight extends SvrProcess{
	int p_PP_Order_ID=0;
	int p_AD_Org_ID=0;
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();

			
			if (para[i].getParameter() == null)
				;
			else if (name.equals("PP_Order_ID"))
				p_PP_Order_ID = para[i].getParameterAsInt();
			else if (name.equals("AD_Org_ID"))
				p_AD_Org_ID = para[i].getParameterAsInt();

			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		
		//creamos una pesada sin info si no existe
		X_PP_Order pporder=new X_PP_Order(getCtx(),p_PP_Order_ID,get_TrxName());
		
		X_PP_Order pporderfrom = new Query(getCtx(), X_PP_Order.Table_Name, "IsActive='Y' and ad_org_id=?", get_TrxName()).setParameters(p_AD_Org_ID).setClient_ID().setOrderBy(X_PP_Order.COLUMNNAME_PP_Order_ID).first();
		//log.severe("pporder desde: "+pporderfrom.get_ID());
		
		if(p_PP_Order_ID==0) {
			X_PP_Order.copyValues(pporderfrom, pporder);
			pporder.setAD_Org_ID(pporderfrom.getAD_Org_ID());
			if (p_AD_Org_ID>0) {
				pporder.setAD_Org_ID(p_AD_Org_ID);
			}
			pporder.setDateOrdered(new Timestamp(System.currentTimeMillis()));
			pporder.setDocumentNo("");
			pporder.set_ValueOfColumn("Weight", BigDecimal.ZERO);
			pporder.set_ValueOfColumn("Weight2", BigDecimal.ZERO);
			pporder.set_ValueOfColumn("Weight3", BigDecimal.ZERO);
			
		}
		BigDecimal weight=(BigDecimal)pporder.get_Value("Weight");
		BigDecimal weight2=(BigDecimal)pporder.get_Value("Weight2");
		BigDecimal weight3=(BigDecimal)pporder.get_Value("Weight3");
		
		/*if(weight.compareTo(BigDecimal.ZERO)!=0 && weight2.compareTo(BigDecimal.ZERO)!=0) {
			pporder.set_ValueOfColumn("Weight", BigDecimal.ZERO);
			pporder.set_ValueOfColumn("Weight2", BigDecimal.ZERO);
			pporder.set_ValueOfColumn("Weight3", BigDecimal.ZERO);
			
		}*/
		pporder.set_ValueOfColumn("ReferenceNo","IP");
		pporder.saveEx();
		//DB.executeUpdate("UPDATE PP_Order SET ReferenceNo='IP' where PP_Order_ID=1057326", get_TrxName());
		commitEx();
		//desarrollando
		
		boolean got=false;

		

		String sql = "SELECT PP_Order_ID,Weight,Weight2,0.00 Weight3,case when DateFrom is null then now() else DateFrom end DateFrom, AD_Org_ID from PP_Order where PP_Order_ID="+pporder.get_ID()+" AND COALESCE(ReferenceNo,'')='' ";
		if (p_AD_Org_ID>0) {
			sql=sql+"AND ad_org_id="+p_AD_Org_ID;
		}
		//log.severe(sql);
		StringWriter sw = new StringWriter();
		StreamResult sr = new StreamResult(sw);
		
		int i=0;
		do {
			if(i==10) { got=true; pporder.set_ValueOfColumn("ReferenceNo","");
			pporder.saveEx();}
			try
			{
				PreparedStatement pstmt = null;
				pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
				ResultSet rs = null;
				rs = pstmt.executeQuery();
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.newDocument();
				while (rs.next()) {
					
					Element results = doc.createElement("Results");
					doc.appendChild(results);
					got=true;
					Element row = doc.createElement("Row");
					results.appendChild(row);
					List<String> lcolumname= Arrays.asList("PP_Order_ID","Weight","Weight2","Weight3","DateFrom","Quintal","Quintal2","Quintal3","AD_Org_ID");
					for (String columnName : lcolumname) {
						//log.severe(columnName);
						if(columnName.compareTo("Quintal")==0) {
							double Weight=rs.getDouble("Weight");
							double value=Weight/45.359237;

							Element node = doc.createElement(columnName);
							node.appendChild(doc.createTextNode(String.valueOf(value)));
							row.appendChild(node);



						}else if(columnName.compareTo("Quintal2")==0) {
							double Weight2=rs.getDouble("Weight2");
							double value=Weight2/45.359237;

							Element node = doc.createElement(columnName);
							node.appendChild(doc.createTextNode(String.valueOf(value)));
							row.appendChild(node);



						}else if(columnName.compareTo("Quintal3")==0) {
							double Weight3=rs.getDouble("Weight3");
							double value=Weight3/45.359237;

							Element node = doc.createElement(columnName);
							node.appendChild(doc.createTextNode(String.valueOf(value)));
							row.appendChild(node);



						}else {
							Object value = rs.getObject(columnName);
							Element node = doc.createElement(columnName);
							node.appendChild(doc.createTextNode(value.toString()));
							row.appendChild(node);
						}


					}

				}
				DOMSource domSource = new DOMSource(doc);
				TransformerFactory tf = TransformerFactory.newInstance();
				Transformer transformer = tf.newTransformer();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");


				transformer.transform(domSource, sr);
				
			}
			catch (Exception e)
			{
				log.log(Level.SEVERE, sql.toString(), e);
			}
			if(!got) {
				TimeUnit.SECONDS.sleep(1);
				i=i+1;
				this.statusUpdate("Esperando por pesada: "+i+" segundos");
				
			}
		}while(!got);
		return sw.toString();
	}

}
